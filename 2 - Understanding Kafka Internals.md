## Kafka Internals

# Taking a closer look

## Brokers
Broker configuration is important in Kafka. In this lesson we'll go through the changes one could make to customize the configuration. Also, we'll explore the resiliency of Kafka in the event of broker failure. Zookeeper shell commands
```
bin/zookeeper-shell.sh zookeeper:2181/kafka
ls /
```

### Broker Configuration Changes

```sh
#set to false in production
auto.create.topics.enable=true
#this number will always be broker 1, if replaced, keep 1
broker.id=1
#I would set to false, but up to you
delete.topic.enable=true
#at least 2 to prevent data loss if broker goes down
default.replication.factor=3
#depending on the size of disk, may need to descrease this
log.retention.hours=168
#set to 2, meaning 2 copies of your data on separate nodes
min.insync.replicas=2
# at least 3 for three broker, to spread messages across brokers
num.partitions=7
```

## Replicas

Kafka places a high importance on the replication of data, ensuring that there is zero data loss. You can, however, turn off replication (even after the topic as been created). In this lesson, we explore how the data is replicated and how to ensure that there are no lost messages in your Kafka cluster.

## Handling Requests

There are a few ways to process requests that come into the Broker. In this lesson, we'll talk about all of the various settings you can change to process messages differently, and why you would want to use each one.

## Partitions

Once you've created a topic, there are many ways you can modify the partitions and elect new replica leaders. In this lesson, I'll show you a few tools that modify your partitions after topic creation.

### Output the data from the partition log
```
./bin/kafka-run-class.sh kafka.tools.DumpLogSegments --print-data-log --files /data/kafka/test5-6/00000000000000000000.log
```
### Rotate the logs
```
./bin/kafka-configs.sh --zookeeper zookeeper1:2181/kafka --alter --entity-type topics --entity-name test --add-config segment.ms=60000
```
### Open a producer to send messages
```
./bin/kafka-console-producer.sh --broker-list kafka1:9092 --topic test
```
### Remove the log rotation setting
```
./bin/kafka-configs.sh --zookeeper zookeeper1:2181/kafka --alter --entity-type topics --entity-name test --delete-config segment.ms
```
### Create the json file to reassign partitions
```
{"topics":
 [{"topic": "test"}],
 "version":1
}
```
### Run the command through a dry-run
```
./bin/kafka-reassign-partitions.sh --zookeeper zookeeper1:2181/kafka --generate \
--topics-to-move-json-file topics.json \
--broker-list 3,2,1
```
### Execute the reassignment
```
./bin/kafka-reassign-partitions.sh --zookeeper zookeeper1:2181/kafka --execute \
--reassignment-json-file plan.json 
```
### Verify the reassignment completed
```
./bin/kafka-reassign-partitions.sh --zookeeper zookeeper1:2181/kafka --verify \
--reassignment-json-file plan.json
```
### Change the replication factor
```
{"partitions":
 [{"topic": "test", "partition": 0,
 "replicas": [
 2,
 1
 ]
 }
 ],
 "version":1
}
```
### Execute the replication factor change
```
./bin/kafka-reassign-partitions.sh --zookeeper zookeeper1:2181/kafka --execute \
--reassignment-json-file replicacount.json
```
### Describe the topic and see the change
```
./bin/kafka-topics.sh --zookeeper zookeeper1:2181/kafka --topic test --describe
```
### Re-assign the leader replica
```
./bin/kafka-preferred-replica-election.sh --zookeeper zookeeper1:2181/kafka
```

# Data Delivery

## Reliability
Due to the nature of Kafka, reliability is a huge concern. Depending on the application, you could accidentally lose messages or duplicate message, causing some real problems for end users. Kafka allows you to choose between speed and reliability, and we'll go through how to do that in this lesson.
- ACID complaint: Atomicity, Consistency, Isolation, Durability
- acks : acknowledgements

## Integrity
To make sure the consumer is receiving all of the messages successully, you can control polling options: offset, reset, and more. In this lesson, we'll go through all of those configurations and I'll show you an example of how to set the configurations in your Java application.

## Security
Up until this point, we've been accessing a cluster that's not secure. To make sure that only certain users have the ability to change topics and the cluster configuration, authentication, and authorization is used. To ensure that your cluster access is secure, we can use SSL or SASL. For encrypting our data between clients, brokers, and outside servers, we can use encryption in transit. In this lesson, we'll go over what this version of Kafka supports. Kafka Documentation https://kafka.apache.org/documentation/#security

## Data Types
With the different types of data coming in and out of Kafka clusters, schemas help with the consistency of that data over time. You may never know what consumers are trying to subscribe to the messages, so it is wise to use a tool like Avro to make sure the consumers don't miss any data within the messages themselves. In this lesson we'll setup the Avro tool and prepare you for managing schemas over the life of the Kafka cluster.

# Producers and Consumers

## Developing Applications for Kafka

### Download Maven.
```
curl -O https://www-us.apache.org/dist/maven/maven-3/3.6.1/binaries/apache-maven-3.6.1-bin.tar.gz
```
Unzip the file, move the contents, and add Maven to your path.
```
tar xzvf apache-maven-3.6.1-bin.tar.gz
sudo mv apache-maven-3.6.1 /opt
export PATH=/opt/apache-maven-3.6.1/bin:$PATH
```
Create your project folder and change into that directory.
```
mkdir kafka-project
cd kafka-project
```
Create a Maven project.
```
mvn -B archetype:generate \
  -DarchetypeGroupId=org.apache.maven.archetypes \
  -DgroupId=com.github.chadmcrowell \
  -DartifactId=kafka-app
```
Add the following to the pom.xml file.
```
<!-- https://mvnrepository.com/artifact/org.apache.kafka/kafka-clients -->
<dependency>
  <groupId>org.apache.kafka</groupId>
  <artifactId>kafka-clients</artifactId>
  <version>2.2.1</version>
</dependency>

<!-- https://mvnrepository.com/artifact/org.slf4j/slf4j-simple -->
<dependency>
  <groupId>org.slf4j</groupId>
  <artifactId>slf4j-simple</artifactId>
  <version>1.7.26</version>
</dependency>
```
Compile the application sources.
```
mvn compile
```
Change the App.java file to Producer.java and change the public class.
```java
package com.github.chadmcrowell;

public class Producer {
        public static void main(String[] args) {
                System.out.println("hello world");
        }
}
```
Compile the application sources again.
```
mvn compile
```
Test your "hello world" app (from the kafa-app dir).
```
mvn exec:java -Dexec.mainClass="com.github.chadmcrowell.Producer"
```
Add the producer configs to your Producer.java file.
```java
package com.github.chadmcrowell;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;

public class Producer {
        public static void main(String[] args) {
                String bootstrapServers = "kafka1:9092";
                // Producer configs
                Properties properties = new Properties();
                properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
                properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
                properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

                // create producer
                KafkaProducer<String, String> producer = new KafkaProducer<String, String>(properties);

                // create producer record
                ProducerRecord<String, String> record = new ProducerRecord<String, String>("test", "hello world");

                // send data
                producer.send(record);

                // flush data
                producer.flush();

                // close producer
                producer.close();

        }
}
```
Compile the application sources yet again.
```
mvn compile
```
Run the console consumer in another terminal session.
```
bin/kafka-console-consumer.sh --bootstrap-server kafka1:9092 --topic test --group application4
```
Run the producer application.
```
mvn exec:java -Dexec.mainClass="com.github.chadmcrowell.Producer"
```

## Kafka Connect
Kafka Connect is an API that comes with Kafka. It's a popular tool and developers have already built connectors for many different data sources. Even if a client already exists in your Kakfa cluster, Kafka Connect can insert additional data for processing inside your Kafka cluster. this lessong covers using Kafka Connect in standalone mode.

## File Source and File Sink
Sinks are the connects we use to get our data out of the Kafka Cluster using Kafka Connect. There is an Elasticsearch sink, a Hadoop sink, and a file sink. In this lesson, we continue our work with Kafka connect and demonstrate a file sink in real-time.

# Hands-On Lab: Using Kafka Connect to Capture Data from a Relational Database

## Additional Information and Resources
In this hands-on lab, we use a container, installed with all the necessary Kafka Connect components. We open a shell to that container and create a SQLite database with a new table to represent the data. We then start Kafka Connect in standalone mode and create a connector for the SQLite database. We verify the connector is working by writing additional tables to the database while monitoring the Kafka consumer.

We must adhere to the following rules:

- We use the docker image confluentinc/docker-demo-base:3.3.0
- The name of the container is "sqlite-test".
- The name of the database is "test.db".
- The name of the database table is "accounts".
- We initially insert at least two names in the table.
- We use the schema connect-avro-standalone.properties.
- We use the source-quickstart-sqlite.properties connector properties.
- After the connector has been established, we write at least two more values to the accounts table and display them using a Kafka consumer.

## Learning Objectives
### Start the Docker Container and Open a New Shell Inside
  
  Run the following command to start the container and connect to it.

```
sudo docker run -ti --rm --name sqlite-demo --network host confluentinc/docker-demo-base:3.3.0
```

### Start Kafka

  Start Kafka with the following command.

```
cd /tmp
confluent start
```

### Install SQLite3 and Create a New Database Table

  Install SQLite3 with the following command.

```
apt-get install sqlite3
```

### Create a new database and table with the following command.

```
sqlite3 test.db

CREATE TABLE accounts (
id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
name VARCHAR(255));
```

### Insert at Least Two New Values into Your New Database Table
  
  Insert the values into your database table using the following command.

```
INSERT INTO accounts(name) VALUES('chad');
INSERT INTO accounts(name) VALUES('terry');
.quit
```

### Start Kafka Connect in Standalone Mode

  Use the following command to stop Kafka Connect from running in distributed mode.

```
confluent stop connect
```

Use the following commnand to start Kafka Connect in standalone mode.
```
connect-standalone -daemon /etc/schema-registry/connect-avro-standalone.properties /etc/kafka-connect-jdbc/source-quickstart-sqlite.properties
```

### Verify the Connectors and Topic Have Been Created

  Check the logs to see that the connector is finished creating.
```
cat /logs/connectStandalone.out | grep -i "finished"
```

  List the connectors.

```
curl -s localhost:8083/connectors
```

  Describe the topic created by the connector.

```
kafka-topics --list --zookeeper localhost:2181 | grep test-sqlite-jdbc
```

### Start a Kafka Consumer and Write New Data to the Database

  Start a new Console Consumer:

```
kafka-avro-console-consumer --new-consumer --bootstrap-server localhost:9092 --topic test-sqlite-jdbc-accounts --from-beginning
```

  In a new terminal, write some new values to the database table.

```
sudo docker exec -it sqlite-demo //bin//bash

cd /tmp
sqlite3 test.db
INSERT INTO accounts(name) VALUES('william');
.quit
```
