## Kafka Administration

# Topic Administration

# Topic Tools

Now that we know how to create a topic, let's discover some of the more advanced tools for modifying topic properties. In this lesson, we go through a number of commands and show you ways to modify your topic and broker configurations using the ```kafka-topics``` and ```kafka-configs``` tools.

## Kafka Documentation - Topic Level Configs
https://kafka.apache.org/documentation/#topicconfigs

## Commands Used in This Lesson
```
# Create a topic if a topic with the same name does NOT exist
bin/kafka-topics.sh --zookeeper zookeeper1:2181/kafka \
--create \
--topic topic-1 \
--replication-factor 1 \
--partitions 3 \
--if-not-exists

# Alter the number of partitions (can only go up)
bin/kafka-topics.sh --zookeeper zookeeper1:2181/kafka \
--alter \
--topic topic-1 \
--partitions 7

# Delete a topic (this is irreversible)
bin/kafka-topics.sh --zookeeper zookeeper1:2181/kafka \
--delete \
--topic topic-1

# List all topics
bin/kafka-topics.sh --zookeeper zookeeper1:2181/kafka \
--list

# Describe all the topics at once
bin/kafka-topics.sh --zookeeper zookeeper1:2181/kafka \
--describe

# Identify any overrides to topics (configs added to the defaults)
bin/kafka-topics.sh --zookeeper zookeeper1:2181/kafka \
--describe \
--topics-with-overrides

# Topics that are not in-sync with all replicas
bin/kafka-topics.sh --zookeeper zookeeper1:2181/kafka \
--describe \
--under-replicated-partitions

# Topics without a leader replica
bin/kafka-topics.sh --zookeeper zookeeper1:2181/kafka \
--describe \
--unavailable-partitions

# Describe the configurations for all topics (only in addition to the defaults)
bin/kafka-configs.sh --zookeeper zookeeper1:2181/kafka \
--describe \
--entity-type topics

# Describe the configurations for a specific topic (defaults will not show)
bin/kafka-configs.sh --zookeeper zookeeper1:2181/kafka \
--describe \
--entity-type topics \
--entity-name topic-1

# Change the topics message retention
bin/kafka-configs.sh --zookeeper zookeeper1:2181/kafka \
--alter \
--entity-type topics \
--entity-name connect-test \
--add-config retention.ms=3600000

# Describe the configurations for all brokers (defaults will not show)
bin/kafka-configs.sh --zookeeper zookeeper1:2181/kafka \
--entity-type brokers \
--entity-default \
--describe

# Describe the configuration for broker 0 (defaults will not show)
bin/kafka-configs.sh --zookeeper zookeeper1:2181/kafka \
--entity-type brokers \
--entity-name 0 \
--describe

# Add a custom config to broker 0 that will change it's log cleaner thread count
bin/kafka-configs.sh --zookeeper zookeeper1:2181/kafka \
--entity-type brokers \
--entity-name 0 \
--alter \
--add-config log.cleaner.threads=2

# Remove all custom configs (not including defaults) from broker 0
bin/kafka-configs.sh --zookeeper zookeeper1:2181/kafka \
--entity-type brokers \
--entity-name 0 \
--alter \
--delete-config log.cleaner.threads
```

# Topic Configurations

You might find it difficult to change the configuration of an existing cluster with millions of messages being produced and consumed. In this lesson, we take a look at the ```kafka-consumer-groups``` command and how to change the configuration of our brokers and topics while the cluster is up and running.

## Kafka Documentation - Managing Consumer Groups
https://kafka.apache.org/documentation/#basic_ops_consumer_group


## Commands Used in This Lesson

```
# List all the consumer groups
bin/kafka-consumer-groups.sh --bootstrap-server kafka1:9092 \
--list

# Describe a specific consumer group
bin/kafka-consumer-groups.sh --bootstrap-server kafka1:9092 \
--describe \
--group application1

# Describe the active members of the group 
bin/kafka-consumer-groups.sh --bootstrap-server kafka1:9092 \
--describe \
--group application1 \
--members

# If the group has active members, get a more verbose output
bin/kafka-consumer-groups.sh --bootstrap-server kafka1:9092 \
--describe \
--group application1 \
--members \
--verbose

# Describe the state of the group
bin/kafka-consumer-groups.sh --bootstrap-server kafka1:9092 \
--describe \
--group application1 \
--state

# Delete a consumer group (only works if there are no active members). 
bin/kafka-consumer-groups.sh --bootstrap-server kafka1:9092 \
--delete \
--group application1

# Delete multiple consumer groups
bin/kafka-consumer-groups.sh --bootstrap-server kafka1:9092 \
--delete \
--group application1 \
--group application2

# Reset offsets for a consumer group
bin/kafka-consumer-groups.sh --bootstrap-server kafka:9092 \
--reset-offsets \
--group application1 \
--topic topic-1 \
--to-latest
```

# Message Behavior

Within a cluster, messages may not be comsumed properly or may be duplicated for various reasons. It's a good idea to regularly check to see if message offsets are being committed. In this lesson, we discuss preventative maintance for messages and what to do if you experience broker failure during partition reasignment.

## Check If Message Offsets Occur
```
bin/kafka-console-consumer.sh --bootstrap-server kafka1:9092 \
--topic __consumer_offsets \
--formatter 'kafka.coordinator.group.GroupMetadataManager$OffsetsMessageFormatter' \
--max-messages 1
``` 

## Modify the Message Reading Capability in Kafka
```java
/*
 * Copyright 2018 Confluent Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.confluent.kafka.formatter;

import org.apache.avro.AvroRuntimeException;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.DecoderFactory;
import org.apache.avro.util.Utf8;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.config.ConfigException;
import org.apache.kafka.common.errors.SerializationException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import io.confluent.kafka.schemaregistry.client.CachedSchemaRegistryClient;
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import kafka.common.KafkaException;
import kafka.common.MessageReader;
import io.confluent.kafka.serializers.AbstractKafkaAvroSerializer;

/**
 * Example
 * To use AvroMessageReader, first make sure that Zookeeper, Kafka and schema registry server are
 * all started. Second, make sure the jar for AvroMessageReader and its dependencies are included
 * in the classpath of kafka-console-producer.sh. Then run the following
 * command.
 *
 * <p>1. Send Avro string as value. (make sure there is no space in the schema string)
 * bin/kafka-console-producer.sh --broker-list localhost:9092 --topic t1 \
 *   --line-reader io.confluent.kafka.formatter.AvroMessageReader \
 *   --property schema.registry.url=http://localhost:8081 \
 *   --property value.schema='{"type":"string"}'
 *
 * <p>In the shell, type in the following.
 * "a"
 * "b"
 *
 * <p>2. Send Avro record as value.
 * bin/kafka-console-producer.sh --broker-list localhost:9092 --topic t1 \
 *   --line-reader io.confluent.kafka.formatter.AvroMessageReader \
 *   --property schema.registry.url=http://localhost:8081 \
 *   --property value.schema='{"type":"record","name":"myrecord","fields":[{"name":"f1","type":"string"}]}'
 *
 * <p>In the shell, type in the following.
 * {"f1": "value1"}
 *
 * <p>3. Send Avro string as key and Avro record as value.
 * bin/kafka-console-producer.sh --broker-list localhost:9092 --topic t1 \
 *   --line-reader io.confluent.kafka.formatter.AvroMessageReader \
 *   --property schema.registry.url=http://localhost:8081 \
 *   --property parse.key=true \
 *   --property key.schema='{"type":"string"}' \
 *   --property value.schema='{"type":"record","name":"myrecord","fields":[{"name":"f1","type":"string"}]}'
 *
 * <p>In the shell, type in the following.
 * "key1" \t {"f1": "value1"}
 *
 */
public class AvroMessageReader extends AbstractKafkaAvroSerializer implements MessageReader {

  private String topic = null;
  private BufferedReader reader = null;
  private Boolean parseKey = false;
  private String keySeparator = "\t";
  private boolean ignoreError = false;
  private final DecoderFactory decoderFactory = DecoderFactory.get();
  private Schema keySchema = null;
  private Schema valueSchema = null;
  private String keySubject = null;
  private String valueSubject = null;

  /**
   * Constructor needed by kafka console producer.
   */
  public AvroMessageReader() {
  }

  /**
   * For testing only.
   */
  AvroMessageReader(
      SchemaRegistryClient schemaRegistryClient, Schema keySchema, Schema valueSchema,
      String topic, boolean parseKey, BufferedReader reader, boolean autoRegister
  ) {
    this.schemaRegistry = schemaRegistryClient;
    this.keySchema = keySchema;
    this.valueSchema = valueSchema;
    this.topic = topic;
    this.keySubject = topic + "-key";
    this.valueSubject = topic + "-value";
    this.parseKey = parseKey;
    this.reader = reader;
    this.autoRegisterSchema = autoRegister;
  }

  @Override
  public void init(java.io.InputStream inputStream, java.util.Properties props) {
    topic = props.getProperty("topic");
    if (props.containsKey("parse.key")) {
      parseKey = props.getProperty("parse.key").trim().toLowerCase().equals("true");
    }
    if (props.containsKey("key.separator")) {
      keySeparator = props.getProperty("key.separator");
    }
    if (props.containsKey("ignore.error")) {
      ignoreError = props.getProperty("ignore.error").trim().toLowerCase().equals("true");
    }
    reader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
    String url = props.getProperty(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG);
    if (url == null) {
      throw new ConfigException("Missing schema registry url!");
    }

    Map<String, Object> originals = getPropertiesMap(props);

    schemaRegistry = new CachedSchemaRegistryClient(
        url, AbstractKafkaAvroSerDeConfig.MAX_SCHEMAS_PER_SUBJECT_DEFAULT, originals);
    if (!props.containsKey("value.schema")) {
      throw new ConfigException("Must provide the Avro schema string in value.schema");
    }
    String valueSchemaString = props.getProperty("value.schema");
    Schema.Parser parser = new Schema.Parser();
    valueSchema = parser.parse(valueSchemaString);

    if (parseKey) {
      if (!props.containsKey("key.schema")) {
        throw new ConfigException("Must provide the Avro schema string in key.schema");
      }
      String keySchemaString = props.getProperty("key.schema");
      keySchema = parser.parse(keySchemaString);
    }
    keySubject = topic + "-key";
    valueSubject = topic + "-value";
    if (props.containsKey("auto.register")) {
      this.autoRegisterSchema = Boolean.valueOf(props.getProperty("auto.register").trim());
    } else {
      this.autoRegisterSchema = true;
    }
  }

  private Map<String, Object> getPropertiesMap(Properties props) {
    Map<String, Object> originals = new HashMap<>();
    for (final String name: props.stringPropertyNames()) {
      originals.put(name, props.getProperty(name));
    }
    return originals;
  }

  @Override
  public ProducerRecord<byte[], byte[]> readMessage() {
    try {
      String line = reader.readLine();
      if (line == null) {
        return null;
      }
      if (!parseKey) {
        Object value = jsonToAvro(line, valueSchema);
        byte[] serializedValue = serializeImpl(valueSubject, value);
        return new ProducerRecord<>(topic, serializedValue);
      } else {
        int keyIndex = line.indexOf(keySeparator);
        if (keyIndex < 0) {
          if (ignoreError) {
            Object value = jsonToAvro(line, valueSchema);
            byte[] serializedValue = serializeImpl(valueSubject, value);
            return new ProducerRecord<>(topic, serializedValue);
          } else {
            throw new KafkaException("No key found in line " + line);
          }
        } else {
          String keyString = line.substring(0, keyIndex);
          String valueString = (keyIndex + keySeparator.length() > line.length())
                               ? ""
                               : line.substring(keyIndex + keySeparator.length());
          Object key = jsonToAvro(keyString, keySchema);
          byte[] serializedKey = serializeImpl(keySubject, key);
          Object value = jsonToAvro(valueString, valueSchema);
          byte[] serializedValue = serializeImpl(valueSubject, value);
          return new ProducerRecord<>(topic, serializedKey, serializedValue);
        }
      }
    } catch (IOException e) {
      throw new KafkaException("Error reading from input", e);
    }
  }

  private Object jsonToAvro(String jsonString, Schema schema) {
    try {
      DatumReader<Object> reader = new GenericDatumReader<Object>(schema);
      Object object = reader.read(null, decoderFactory.jsonDecoder(schema, jsonString));

      if (schema.getType().equals(Schema.Type.STRING)) {
        object = ((Utf8) object).toString();
      }
      return object;
    } catch (IOException e) {
      throw new SerializationException(
          String.format("Error deserializing json %s to Avro of schema %s", jsonString, schema), e);
    } catch (AvroRuntimeException e) {
      throw new SerializationException(
          String.format("Error deserializing json %s to Avro of schema %s", jsonString, schema), e);
    }
  }

  @Override
  public void close() {
    // nothing to do
  }
}
```

## Specify the --line-reader Option
```
bin/kafka-console-producer.sh --broker-list kafka1:9092 --topic topic-1 \
--line-reader io.confluent.kafka.formatter.AvroMessageReader \
--property schema.registry.url=http://localhost:8081 \
--property value.schema='{"type":"string"}'
```

## Delete the reassign_parftitions Node
```
bin/zookeeper-shell.sh zookeeper1:2181/kafka

ls /admin
delete /admin/reassign_partitions
```

## Delete the Controller Node
```
bin/zookeeper-shell.sh zookeeper1:2181/kafka

ls /
delete /controller
```

# Hands-On Lab: Creating a Topic with Custiom Configurations in Kafka

## Description

There are many custom configurations that we can apply to topics in Kafka. In this hands-on lab, we'll go through creating a topic, applying a custom configuration to that topic, and then testing the custom configuration by alerting the state of the partitions tied to that topic.

## Learning Objectives

### Set Up the Cluster
Use Docker Compose to build the Kafka Cluster:
```
cd content-kafka-deep-dive

docker-compose up -d --build
```
Install Java:
```
sudo apt install default-jdk
```
Unzip and change into the Kafka binaries directory:
```
tar -xvf kafka_2.12-2.2.0.tgz && mv kafka_2.12-2.2.0/ kafka
```

### Create a Topic with Three Partitions and a Replication Factor of `3`

Create a topic named ```transaction```:
```
bin/kafka-topics.sh --zookeeper localhost:2181 \
--create \
--topic transaction \
--replication-factor 3 \
--partitions 3
```

### Add a Custom Configuration to the Topic

Add the custom configuration min.insync.replicas=3 to the topic transaction:
```
bin/kafka-configs.sh --zookeeper localhost:2181 \
--alter \
--entity-type topics \
--entity-name transaction \
--add-config min.insync.replicas=3
```

Verify the topic configuration applied:
```
bin/kafka-configs.sh --zookeeper localhost:2181 \
--describe \
--entity-type topics \
--entity-name transaction
```

### Change the Replica Count for the Topic

First off, let's create a json file named replicacount.json with these contents:
```json
{"partitions":
 [{"topic": "transaction", "partition": 0,
 "replicas": [
 2
 ]
 }
 ],
 "version":1
}
```
Now we can execute the replica count change but using that json file:
```
bin/kafka-reassign-partitions.sh --zookeeper localhost:2181 \
--execute \
--reassignment-json-file replicacount.json
```
Once we get a "Successfully started..." message, let's describe the topic to see the replica change:
```
bin/kafka-topics.sh --zookeeper localhost:2181 \
--topic transaction \
--describe
```

### Run a Producer to get an error message

Open a producer and send some messages to your topic
```
bin/kafka-console-producer.sh --broker-list localhost:9092 \
--topic transaction \
--producer-property acks=all
```

# Storage Administration

## File Formats and Indexes

In the ```/data/kafka``` directory are our message logs. As these logs grow over time, both finding and maintaining these logs is handled by Kafka. By default, the messages are kept for one week and further compaction can occur. In this lesson, we cover how to set up log compaction for your messages.

Create a topic with log compaction:

```
bin/kafka-topics.sh --zookeeper zookeeper1:2181/kafka \
--create \
--topic transaction \
--partitions 1 \
--replication-factor 1 \
--config cleanup.policy=compact \
--config min.cleanable.dirty.ratio=0.001 \
--config segment.ms=5000
```

Describe the topic to see the configuration:
```
bin/kafka-topics.sh --zookeeper zookeeper1:2181/kafka \
--topic transaction \
--describe
```
Open a consumer to the topic:
```
bin/kafka-console-consumer.sh --bootstrap-server kafka1:9092 \
--topic transaction \
--from-beginning \
--property print.key=true \
--property key.separator=,
```
Open a producer to write messages to Kafka:
```
bin/kafka-console-producer.sh --broker-list kafka1:9092 \
--topic transaction \
--property parse.key=true \
--property key.separator=,
```

## File Management

## Storage Structures

# Hands-On Lab: Backup messages to an S3 Bucket in Kafka

## Description

Kafka is known for replicating data amongst brokers, to account for broker failure. Sometimes, this isn't enough, or perhaps you are sharing this data with a third-party application. Backing up your messages to an alternate source can save a lot of time and energy when trying to configure access to a cluster, or delivering that data quickly across the world.

## Additional Information and Resources
In this hands-on lab, we will be sending topic messages to Amazon S3 using Kafka Connect. First, we'll create an S3 bucket, using some provided commands. Then we'll produce to a new topic, then use the connect plugin to verify that the data got copied to the newly created S3 bucket.

No previous Amazon Web Services knowledge is required. The purpose of this exercise is to learn how to use Kafka commands for sending data to an outside data repository. We will meet the following requirements:

- Create the S3 bucket in the us-east-1 region and give it a unique name (use all lowercase letters, NO underscores)
- Use the AWS credentials given with this hands-on lab
- Start the Kafka cluster using the confluent start command
- Create the topic name s3_topic and include a simple schema in Kafka
- Import at least 9 records into the Kafka cluster
- Verify that the records exist in the S3 bucket by listing the contents

## Learning Objectives

### Connect and Start the Kafka Cluster

In the Bastion host, start a container and open a shell to that container:

```
sudo docker run -ti --rm --name kafka-cluster --network host confluentinc/docker-demo-base:3.3.0
```
Get into the /tmp directory
```
cd /tmp
```
Now we can start up the Kafka cluster:
```
confluent start
```
### Create a New S3 Bucket
Install the awscli tool (once we've updated our system):
```
apt update
apt install -y awscli
```
Configure access to AWS by creating a key. Note that our cloud_user Access and Super Access keys are sitting back on the hands-on lab page:
```
aws configure

AWS Access Key ID: [ACCESS_KEY]
AWS Secret Access Key: [SECRET_ACCESS_KEY]
Default region name: us-east-1
Default output format: [None]
```
Create a new bucket in the us-east-1 region. Make sure our name is unique (all lowercase and NO underscores):

```
aws s3api create-bucket --region us-east-1 --bucket [UNIQUE_BUCKET_NAME]
```
Add the new bucket name to the configuration file for the S3 connector:
```
apt install -y vim
```
Then exit the properties file:
```
vim /etc/kafka-connect-s3/quickstart-s3.properties
```
Change the region and bucket.name lines to this:
```
s3.region=us-east-1
s3.bucket.name=[$globally_unique_bucket_name]
```

### Start a Producer to a New Topic Named s3_topic and Write at Least 9 Messages
Now let's open an Avro console producer to the topic, and include a schema:
```
kafka-avro-console-producer --broker-list localhost:9092 --topic s3_topic --property value.schema='{"type":"record","name":"myrecord","fields":[{"name":"f1","type":"string"}]}'
```
Type the 9 messages following the defined schema
```
{"f1": "value1"}
{"f1": "value2"}
{"f1": "value3"}
{"f1": "value4"}
{"f1": "value5"}
{"f1": "value6"}
{"f1": "value7"}
{"f1": "value8"}
{"f1": "value9"}
```
Press Ctrl + C to exit the session.

### Start the Connector and Verify the Messages Are in the S3 Bucket
We can start the connector and load the configuration:

```
confluent load s3-sink
```
We'll get some JSON output, with our bucket name in there somewhere. Copy that name, and use it in the command to list its objects:
```
aws s3api list-objects --bucket [OUR_BUCKET_NAME]
```

## Stream Processing

### How Streams Work

A stream is much different than a database table. Streams are a series of ongoing sequential events. There are no delete or update abilities because streams are immutable. In this lesson, we go over why streams are important and what purpose they serve.

### Design Patterns

When considering a Kafka streams solution, there are many decisions you must make in order to process events appropriately. In this lesson, we'll go over the six different design patterns and how each applies to specific event-handling scenarios.

### Frameworks

Depending on the problem you're trying to solve, providing a framework for your event streaming application may help you in the long run. The different frameworks available are an important consideration in solving those problems, as we will discuss in this lesson.

# Hands-On Lab: Streaming Data Using Kafka Streams to Count Words

## Description

Kafka Streams is a library enabling you to perform per-event processing of records. You can use it to process data as soon as it arrives, versus having to wait for a batch to occur. In this hands-on lab, we use Kafka Streams to stream some input data as plain-text and process it in real-time. With this, we can count the number of words from our input stream.

## Additional Information and Resources

In this hands-on lab, we use the WordCount demo application that comes with the Kafka binaries. This application is already built, so we won't create the application from scratch. We need to create an input topic, an output topic, and then use the WordCount Streaming Application to count the number of words in the input stream using the Kafka console consumer. We do this by passing in the apprpriate properties to the console consumer to format, serialize, and deserialize the data into the correct output for viewing in the console. When we have an output of the count of each word in the console, we've successfully completed this lab.

We need to create the Kafka cluster in order to proceed with creating our first topic.

Here are the instructions for starting the Kafka cluster:

- Use Docker Compose to build the Kafka Cluster.
```
cd content-kafka-deep-dive

docker-compose up -d --build
```
- Install Java.
```
sudo apt install default-jdk
```

- Unzip the Kafka binaries tar file located in ```/home/cloud_user```
```
tar -xvf kafka_2.12-2.2.0.tgz
```
- Change the name of the 'kakfa_2.12-2.2.0' to 'kafka'
```
mv kafka_2.12-2.2.0 kafka
```
Perform all commands in this hands-on lab from within the ```~/kafka``` directory:

Follow these requirements in order to complete this Hands-on lab:

- Create a topic named ```streams-plaintext-input```.
- Create a topic named ```streams-wordcount-output```.
- Open the Kafka console producer and write the following messages:
  - kafka streams is great
  - kafka processes messages in real time
  - kafka helps real information streams
- Open a Kafka console consumer to the output topic to read messages from the beginning using the default message formatter with the following properties:
  - ```print.key=true```
  - ```print.value=true```
  - ```key.deserializer=org.apache.kafka.common.serialization.StringDeserializer```
  - ```value.deserializer=org.apache.kafka.common.serialization.LongDeserializer```
- Run the ```org.apache.kafka.streams.examples.wordcount.WordCountDemo``` application using the ```kafka-run-class.sh``` command.

## Learning Objectives

### Create the Input and Output Topic
Create the input topic named ```streams-plaintext-input```.
```
bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic streams-plaintext-input
```
Create the output topic named ```streams-wordcount-output```.
```
bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic streams-wordcount-output
```

### Open a Kafka Console Producer
Open a Kafka console producer.

```
bin/kafka-console-producer.sh --broker-list localhost:9092 --topic streams-plaintext-input
```
Type the three messages from the instructions.
```
>kafka streams is great
>kafka processes messages in real time
>kafka helps real information streams
```

### Open a Kafka Console Consumer

Open a Kafka console consumer using the default message formatter and the three properties given in the instructions.
```
bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 \
--topic streams-wordcount-output \
--from-beginning \
--formatter kafka.tools.DefaultMessageFormatter \
--property print.key=true \
--property print.value=true \
--property key.deserializer=org.apache.kafka.common.serialization.StringDeserializer \
--property value.deserializer=org.apache.kafka.common.serialization.LongDeserializer
```

### Run the Kafka Streams Application
Use the kafka-run-class.sh command to run the WordCountDemo application.

```
bin/kafka-run-class.sh org.apache.kafka.streams.examples.wordcount.WordCountDemo
```

# Data Replication

## Multi-Cluster Architectures

There are many different architectures to choose from when mirroring your Kafka cluster. The recommended architecture is active-active, but there are also hub-and-spoke and active-standby. In this lesson, we'll discuss each of them.

## MirrorMaker

MirrorMaker is a tool to replicate your data to a different cluster. This tool comes with the Kafka binaries and requires a lot of configuration for tuning the throughput. You must ensure there is very low latency to produce the synchronization you need. In this lesson, we'll discuss all of the configurations you will use with MirrorMaker.

# Hands-On Lab: Replicating Data Between Two Kafka Clusters

## Additional Information and Resources

In this hands-on lab, we need to create two localized Kafka clusters. These clusters will contain only one zookeeper and kafka instance each. We differentiate between the two by specifying different port numbers and data directories for each cluster. Once both clusters are up and running, we create a topic and replicate that topic to the secondary cluster. We continue to produce messages to the source cluster and ensure that the messages are successfully mirrored.

Use the confluent Kafka binaries for each task located here: https://packages.confluent.io/archive/5.2/confluent-5.2.1-2.12.tar.gz

Use the following requirements to set up your cluster and create your topic:

Your destination cluster must be reachable from Kafka port 9092 and Zookeeper port 2181.
Your source cluster must be reachable from Kafka port 9082 and Zookeeper port 2171.
Your Zookeeper configuration file must be named zookeeper_origin.properties and specify the data directory /tmp/zookeeper_origin.
Your Kafka configuration file must be named server_origin.properties and specify the data directory /tmp/kafka-logs-origin.
Create a topic named test-topic on the source cluster with 1 partition and a replication factor of 1.
Run the replicator tool with Kafka Connect (Standalone Mode) using the quickstart-replicator.properties configuration file.
Verify the topic was replicated to the destination cluster.
Open a console producer and produce some messages to the topic test-topic.
Verify the messages were replicated to the destination cluster by running a console consumer.


## Learning Objectives

- Start the Destination Cluster
  
  Start Zookeeper.
    ```
    bin/zookeeper-server-start etc/kafka/zookeeper.properties
    ```
  Start Kafka.
    ```
    bin/kafka-server-start etc/kafka/server.properties
    ```
- Start the Origin Cluster
  
  Make a copy of the configuration files.
    ```
    cp etc/kafka/zookeeper.properties /tmp/zookeeper_origin.properties
    cp etc/kafka/server.properties /tmp/server_origin.properties
    ```
  
  Change the port numbers for the origin cluster.
    ```
    sed -i '' -e "s/2181/2171/g" /tmp/zookeeper_origin.properties
    sed -i '' -e "s/9092/9082/g" /tmp/server_origin.properties
    sed -i '' -e "s/2181/2171/g" /tmp/server_origin.properties
    sed -i '' -e "s/#listen/listen/g" /tmp/server_origin.properties
    ```
  Change the data directory for the origin cluster.
    ```
    sed -i '' -e "s/zookeeper/zookeeper_origin/g" /tmp/zookeeper_origin.properties
    sed -i '' -e "s/kafka-logs/kafka-logs-origin/g" /tmp/server_origin.properties
    ```
  Start Zookeeper.
    ```
    bin/zookeeper-server-start /tmp/zookeeper_origin.properties
    ```
  Start Kafka.
    ```
    bin/kafka-server-start /tmp/server_origin.properties
    ```
- Create a Topic in the Source Cluster
  
  Create a topic named ```test-topic``` with 1 partition and a replication factor of 1.
  ```
  bin/kafka-topics --create --topic test-topic --replication-factor 1 --partitions 1 --zookeeper localhost:2171
  ```
- Run the Replicator Tool

  Run Kafka Connect in Standalone Mode and pass in the ```quickstart-replicator.properties``` file
  ```
  bin/connect-standalone etc/kafka/connect-standalone.properties etc/kafka-connect-replicator/quickstart-replicator.properties
  ```

- Produce and Verify Replication

  Verify that the topic was replicated to the destination cluster.
  ```
  bin/kafka-topics --describe --topic test-topic.replica --zookeeper localhost:2181
  ```

  Open a Console Producer and write to the topic test-topic in the source cluster.
  ```
  seq 10000 | bin/kafka-console-producer --topic test-topic --broker-list localhost:9082
  ```
  Confirm the messages were replicated by opening a console consumer to the destination cluster.
  ```
  bin/kafka-console-consumer --from-beginning --topic test-topic.replica --bootstrap-server localhost:9092
  ```

# Monitoring

## Cluster and Broker Monitoring

Metrics for the application can be obtained from the JMX. Kafka also uses Yammer Metrics for metrics reporting. In this lesson, we'll go over some common problems at the cluster and host level, as well as discuss potential troubleshooting methods.

Get the JMX port:
```
bin/zookeeper-shell.sh zookeeper1:2181/kafka
get /brokers/ids/3
```
Preferred replica election:
```
bin/kafka-preferred-replica-election.sh --bootstrap-server kafka1:9092 --path-to-json-file topicPartitionList.json
```
Describe under-replicated partitions:
```
bin/kafka-topics.sh --zookeeper zookeeper1:2181/kafka --describe --under-replicated-partitions
```
Reassign partitions:
```
bin/kafka-reassign-partitions.sh --zookeeper zookeeper1:2181/kafka --execute \
--reassignment-json-file replicacount.json
```
- Broker metrics for monitoring:
https://kafka.apache.org/documentation/#monitoring
- Tools from the SRE team at LinkedIn:
https://github.com/linkedin/kafka-tools

## Broker Metrics

In this lesson, we'll discuss the many metrics that you should have in every single one of your monitoring dashboards for Kafka.
- ACTIVE_CONTROLLER_COUNT
- REQUEST_HANDLER_IDLE_RATIO
- ALL_TOPICS_BYTES_IN
- ALL_TOPICS_BYTES_OUT
- ALL_TOPICS_MESSAGES_IN
- PARTITION_COUNT
- LEAD_COUNT
- OFFLINE_PARTITIONS
- REQUEST_METRICS
  
## Java Monitoring

As producers and consumers will most likely be a Java-based application, it is important to monitor the JVM. In this lesson, we'll look at the important beans to include in your monitoring suite, which will allow you to gain important insight into the performance of your Kafka client application.

- Garbage collection