# Kafka Installation

## Installing Kafka using containers

### Add Docker to Your Package Repository
```
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo add-apt-repository    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
```
### Update Packages and Install Docker
```
sudo apt update

sudo apt install -y docker-ce=18.06.1~ce~3-0~ubuntu
```
### Add Your User to the Docker Group
```
sudo usermod -a -G docker cloud_user
```
### Install Docker Compose
```
sudo -i

curl -L https://github.com/docker/compose/releases/download/1.24.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose

chmod +x /usr/local/bin/docker-compose
```
### Clone the Repository That Has Our Docker Compose File
```
git clone https://github.com/linuxacademy/content-kafka-deep-dive.git
```
### Change Directory and Run the Compose YAML File
```
cd content-kafka-deep-dive

docker-compose up -d --build
```
### Install Java
```
sudo apt install -y default-jdk
```
### Get the Kafka Binaries
```
wget http://mirror.cogentco.com/pub/apache/kafka/2.2.0/kafka_2.12-2.2.0.tgz

tar -xvf kafka_2.12-2.2.0.tgz
```
### Create Your First Topic
```
./bin/kafka-topics.sh --zookeeper localhost:2181 --create --topic test --partitions 3 --replication-factor 1
```
### Describe the Topic
```
./bin/kafka-topics.sh --zookeeper localhost:2181 --topic test --describe
```

## Installing Kafka and Zookeeper Using Binaries

### Download the Binaries, and Change the Name of the Directory
```
wget http://mirror.cogentco.com/pub/apache/kafka/2.2.0/kafka_2.12-2.2.0.tgz

tar -xvf kafka_2.12-2.2.0.tgz

mv kafka_2.12-2.2.0 kafka

cd kafka
```

### Install Java
```
sudo apt install -y default-jdk

java -version
Disable RAM Swap
swapoff -a

sudo sed -i '/ swap / s/^/#/' /etc/fstab
```

### Create the Zookeeper Service File
```
sudo vim /etc/init.d/zookeeper
```

### Contents of the ```zookeeper``` File
```
#!/bin/bash
#/etc/init.d/zookeeper
DAEMON_PATH=/home/cloud_user/kafka/bin
DAEMON_NAME=zookeeper
# Check that networking is up.
#[ ${NETWORKING} = "no" ] && exit 0

PATH=$PATH:$DAEMON_PATH

case "$1" in
  start)
        # Start daemon.
        pid=`ps ax | grep -i 'org.apache.zookeeper' | grep -v grep | awk '{print $1}'`
        if [ -n "$pid" ]
          then
            echo "Zookeeper is already running";
        else
          echo "Starting $DAEMON_NAME";
          $DAEMON_PATH/zookeeper-server-start.sh -daemon /home/cloud_user/kafka/config/zookeeper.properties
        fi
        ;;
  stop)
        echo "Shutting down $DAEMON_NAME";
        $DAEMON_PATH/zookeeper-server-stop.sh
        ;;
  restart)
        $0 stop
        sleep 2
        $0 start
        ;;
  status)
        pid=`ps ax | grep -i 'org.apache.zookeeper' | grep -v grep | awk '{print $1}'`
        if [ -n "$pid" ]
          then
          echo "Zookeeper is Running as PID: $pid"
        else
          echo "Zookeeper is not Running"
        fi
        ;;
  *)
        echo "Usage: $0 {start|stop|restart|status}"
        exit 1
esac

exit 0
Change the File's Permissions and Start the Service
sudo chmod +x /etc/init.d/zookeeper
sudo chown root:root /etc/init.d/zookeeper

sudo update-rc.d zookeeper defaults

sudo service zookeeper start
sudo service zookeeper status

```


### Create the Kafka Service File
```
sudo vim /etc/init.d/kafka
```
### Insert the Following Contents into the ```kafka``` File
```
#!/bin/bash
#/etc/init.d/kafka
DAEMON_PATH=/home/cloud_user/kafka/bin
DAEMON_NAME=kafka
# Check that networking is up.
#[ ${NETWORKING} = "no" ] && exit 0

PATH=$PATH:$DAEMON_PATH

# See how we were called.
case "$1" in
  start)
        # Start daemon.
        pid=`ps ax | grep -i 'kafka.Kafka' | grep -v grep | awk '{print $1}'`
        if [ -n "$pid" ]
          then
            echo "Kafka is already running"
        else
          echo "Starting $DAEMON_NAME"
          $DAEMON_PATH/kafka-server-start.sh -daemon /home/cloud_user/kafka/config/server.properties
        fi
        ;;
  stop)
        echo "Shutting down $DAEMON_NAME"
        $DAEMON_PATH/kafka-server-stop.sh
        ;;
  restart)
        $0 stop
        sleep 2
        $0 start
        ;;
  status)
        pid=`ps ax | grep -i 'kafka.Kafka' | grep -v grep | awk '{print $1}'`
        if [ -n "$pid" ]
          then
          echo "Kafka is Running as PID: $pid"
        else
          echo "Kafka is not Running"
        fi
        ;;
  *)
        echo "Usage: $0 {start|stop|restart|status}"
        exit 1
esac

exit 0
```

### Change the Properties of the File and Start the Service
```
sudo chmod +x /etc/init.d/kafka

sudo chown root:root /etc/init.d/kafka

sudo update-rc.d kafka defaults

sudo service kafka start
```

### Stop the ```kafka``` and ```zookeeper``` Services on Each Server
```
sudo service zookeeper stop

sudo service kafka stop
```

### Create the ```server.properties``` File
```
rm config/server.properties

vim config/server.properties
```
### Insert the Following in the Contents of the server.properties File
```
# change this for each broker
broker.id=[broker_number]
# change this to the hostname of each broker
advertised.listeners=PLAINTEXT://[hostname]:9092
# The ability to delete topics
delete.topic.enable=true
# Where logs are stored
log.dirs=/data/kafka
# default number of partitions
num.partitions=8
# default replica count based on the number of brokers
default.replication.factor=3
# to protect yourself against broker failure
min.insync.replicas=2
# logs will be deleted after how many hours
log.retention.hours=168
# size of the log files 
log.segment.bytes=1073741824
# check to see if any data needs to be deleted
log.retention.check.interval.ms=300000
# location of all zookeeper instances and kafka directory
zookeeper.connect=zookeeper1:2181,zookeeper2:2181,zookeeper3:2181/kafka
# timeout for connecting with zookeeeper
zookeeper.connection.timeout.ms=6000
# automatically create topics
auto.create.topics.enable=true
```

### Add the Following to ```/etc/hosts```
```
[server1_private_ip]  kafka1
[server1_private_ip]  zookeeper1
[server2_private_ip]  kafka2
[server2_private_ip]  zookeeper2
[server3_private_ip]  kafka3
[server3_private_ip]  zookeeper3
```

### Create the logs Directory and myid File
```
sudo mkdir -p /data/zookeeper
sudo chown -R cloud_user:cloud_user /data/zookeeper
echo "1" > /data/zookeeper/myid
echo "2" > /data/zookeeper/myid
echo "3" > /data/zookeeper/myid
```
### Create the ```zookeeper.properties``` File
```
rm /home/cloud_user/kafka/config/zookeeper.properties
vim /home/cloud_user/kafka/config/zookeeper.properties
Insert the Following Contents in the zookeeper.properties File
# the directory where the snapshot is stored.
dataDir=/data/zookeeper
# the port at which the clients will connect
clientPort=2181
# setting number of connections to unlimited
maxClientCnxns=0
# keeps a hearbeat of zookeeper in milliseconds
tickTime=2000
# time for inital synchronization
initLimit=10
# how many ticks can pass before timeout
syncLimit=5
# define servers ip and internal ports to zookeeper
server.1=zookeeper1:2888:3888
server.2=zookeeper2:2888:3888
server.3=zookeeper3:2888:3888
```

### Start the Zookeeper and Kafka Services on Each Server
```
sudo service zookeeper start
sudo service kafka start
```

### Create Your First Topic in Your New Kafka Cluster
```
./bin/kafka-topics.sh --zookeeper zookeeper1:2181/kafka --create --topic test --replication-factor 1 --partitions 3
```
```
./bin/kafka-topics.sh --zookeeper zookeeper1:2181/kafka --topic test --describe
```

# Breaking down the commands

Detail for the topics command
```
bin/kafka-topics.sh
```

Creating a topic will all the required arguments
```
bin/kafka-topics.sh --zookeeper zookeeper1:2181/kafka --topic test1 --create --partitions 3 --replication-factor 3
```

Creating a topic including all of the zookeeper servers (not required)
```
bin/kafka-topics.sh --zookeeper zookeeper1:2181,zookeeper2:2181,zookeeper3:2181/kafka --topic test1 --create --partitions 3 --replication-factor 3
```

List all topics
```
bin/kafka-topics.sh --zookeeper zookeeper1:2181/kafka --list
```

Describing a topic
```
bin/kafka-topics.sh --zookeeper zookeeper1:2181/kafka --topic test2 --describe
```

Delete a topic
```
bin/kafka-topics.sh --zookeeper zookeeper1:2181/kafka --topic test2 --delete
```

Detail for the producer command
```
bin/kafka-console-producer.sh
```

Detail for the consumer command
```
bin/kafka-console-consumer.sh
```

Detail for the consumer groups command
```
bin/kafka-consumer-groups.sh
```

# Publishing Messages to a Topic in Kafka

Start a console producer to topic 'test'
```
bin/kafka-console-producer.sh --broker-list kafka1:9092 --topic test
```

Add the acks=all flag to your producer
```
bin/kafka-console-producer.sh --broker-list kafka1:9092 --topic test --producer-property acks=all
```

Create a topic with the console producer (not recommended)
```
bin/kafka-console-producer.sh --broker-list kafka1:9092 --topic test4
```

List the newly created topic
```
bin/kafka-topics.sh --zookeeper zookeeper1:2181/kafka --list
```

View the partitions for a topic
```
bin/kafka-topics.sh --zookeeper zookeeper1:2181/kafka --topic test5 --describe
```

# Subscribing to Messages in a Topic in Kafka

Start a console consumer to a topic
```
bin/kafka-console-consumer.sh --bootstrap-server kafka3:9092 --topic test
```

Consuming messages from the beginning
```
bin/kafka-console-consumer.sh --bootstrap-server kafka3:9092 --topic test --from-beginning
```

# Using Multiple Consumers When Subscribing to Messages

Start a consumer group for a topic
```
bin/kafka-console-consumer.sh --bootstrap-server kafka3:9092 --topic test --group application1
```

Start producing new messages to a topic
```
bin/kafka-console-producer.sh --broker-list kafka1:9092 --topic test
```

Start a consumer group and read messages from the beginning
```
bin/kafka-console-consumer.sh --bootstrap-server kafka3:9092 --topic test --group application1 --from-beginning
```

List the consumer groups
```
bin/kafka-consumer-groups.sh --bootstrap-server kafka3:9092 --list
```

Describe a consumer group
```
bin/kafka-consumer-groups.sh --bootstrap-server kafka3:9092 --describe --group application1
```

# LAB 1 - Install kafka and zookeeper from binaries on 3 servers

Additional Information and Resources
The company you work for would like to start using Apache Kafka for real-time analytics of their application data. They realize the benefit to Kafka, in that it can process millions of messages in real-time. The company would like to start using Kafka as soon as possible and have tasked you with building the Kafka cluster. You will be responsible for building a Kafka cluster with three brokers, and three Zookeeper servers. Your company only gave you three servers total, and would like the cluster configured for high-availability and fault-tolerance. Your boss says that if one of the servers fail, we must ensure that the cluster still remains up and running. That means you must include at least two replicas of the data. Here are the steps you must take to build the Kafka cluster:

- Download the latest Kafka binaries
- Install Java
- Disable RAM swap
- Create a directory with appropriate permissions for Kafka logs
- Create a directory with appropriate permissions for Zookeeper snapshots
- Specify an ID for the Zookeeper servers to reach quorum
- Modify the Kafka configuration file to reflect the appropriate settings
- Modify the Zookeeper configuration file to reflect the appropriate settings
- Create Zookeeper as a service, so it can run without manual intervention
- Create Kafka as a service, so it can run without manual intervention
- Create a topic to test that the cluster is working properly

## Download the Kafka Binaries

Use wget to download the tar file from the mirror:

```
wget http://mirror.cogentco.com/pub/apache/kafka/2.2.0/kafka_2.12-2.2.0.tgz
```
Extract the tar file and move it into the /opt directory:

```
tar -xvf kafka_2.12-2.2.0.tgz
sudo mv kafka_2.12-2.2.0 /opt/kafka
```
Change to the kafka directory and list the contents:
```
cd /opt/kafka
ls
```

## Install Java and Disable RAM Swap

Use the following command to install the latest Java Developer Kit (JDK):
```
sudo apt install -y default-jdk
```
Use the following command to verify that Java has been installed:
```
java -version
```
Use the following command to disable RAM swap:
```
swapoff -a
```
Use the following command to comment out swap in the /etc/fstab file:
```
sudo sed -i '/ swap / s/^/#/' /etc/fstab
```

## Create a New Directory for Kafka and Zookeeper
Use the following command to create a new directory for Kafka message logs:
```
sudo mkdir -p /data/kafka
```
Use the following command to create a snapshot directory for Zookeeper:
```
sudo mkdir -p /data/zookeeper
```
Change ownership of those directories to allow the user cloud_user control:
```
sudo chown -R cloud_user:cloud_user /data/kafka
sudo chown -R cloud_user:cloud_user /data/zookeeper
```
## Specify an ID for Each Zookeeper Server

Use the following command to create a file in /data/zookeeper (on server #1) called myid with the contents "1" to specify Zookeeper server #1:
```
echo "1" > /data/zookeeper/myid
```
Use the following command to create a file in /data/zookeeper (on server #2) called myid with the contents "2" to specify Zookeeper server #2:
```
echo "2" > /data/zookeeper/myid
```
Use the following command to create a file in /data/zookeeper (on server #3) called myid with the contents "3" to specify Zookeeper server #3:
```
echo "3" > /data/zookeeper/myid
```
## Modify the Kafka and Zookeeper Configuration Files
Use the following command to remove the existing server.properties file (in the config directory) and create a new server.properties file:
```
rm config/server.properties
vim config/server.properties
```
Copy and paste the following into the contents of the server.properties file and change the broker.id and the advertised.listeners:
```
# change this for each broker
broker.id=[broker_number]
# change this to the hostname of each broker
advertised.listeners=PLAINTEXT://[hostname]:9092
# The ability to delete topics
delete.topic.enable=true
# Where logs are stored
log.dirs=/data/kafka
# default number of partitions
num.partitions=8
# default replica count based on the number of brokers
default.replication.factor=3
# to protect yourself against broker failure
min.insync.replicas=2
# logs will be deleted after how many hours
log.retention.hours=168
# size of the log files 
log.segment.bytes=1073741824
# check to see if any data needs to be deleted
log.retention.check.interval.ms=300000
# location of all zookeeper instances and kafka directory
zookeeper.connect=zookeeper1:2181,zookeeper2:2181,zookeeper3:2181/kafka
# timeout for connecting with zookeeper
zookeeper.connection.timeout.ms=6000
# automatically create topics
auto.create.topics.enable=true
```
```
broker.id=1
broker.id=2
broker.id=3
advertised.listeners=PLAINTEXT://kafka1:9092
advertised.listeners=PLAINTEXT://kafka2:9092
advertised.listeners=PLAINTEXT://kafka3:9092
```
Use the following command to remove the existing zookeeper.properties file (in the config directory) and create a new zookeeper.properties file:
```
rm config/zookeeper.properties
vim config/zookeeper.properties
```
Copy and paste the following into the contents of the zookeeper.properties file (don't change this file):
```
# the directory where the snapshot is stored.
dataDir=/data/zookeeper
# the port at which the clients will connect
clientPort=2181
# setting number of connections to unlimited
maxClientCnxns=0
# keeps a heartbeat of zookeeper in milliseconds
tickTime=2000
# time for initial synchronization
initLimit=10
# how many ticks can pass before timeout
syncLimit=5
# define servers ip and internal ports to zookeeper
server.1=zookeeper1:2888:3888
server.2=zookeeper2:2888:3888
server.3=zookeeper3:2888:3888
```
## Create the Kafka and Zookeeper Service

Create the file /etc/init.d/zookeeper on each server and paste in the following contents:
```
sudo vim /etc/init.d/zookeeper
```
```
#!/bin/bash
#/etc/init.d/zookeeper
DAEMON_PATH=/opt/kafka/bin
DAEMON_NAME=zookeeper
# Check that networking is up.
#[ ${NETWORKING} = "no" ] && exit 0

PATH=$PATH:$DAEMON_PATH

case "$1" in
  start)
        # Start daemon.
        pid=`ps ax | grep -i 'org.apache.zookeeper' | grep -v grep | awk '{print $1}'`
        if [ -n "$pid" ]
          then
            echo "Zookeeper is already running";
        else
          echo "Starting $DAEMON_NAME";
          $DAEMON_PATH/zookeeper-server-start.sh -daemon /opt/kafka/config/zookeeper.properties
        fi
        ;;
  stop)
        echo "Shutting down $DAEMON_NAME";
        $DAEMON_PATH/zookeeper-server-stop.sh
        ;;
  restart)
        $0 stop
        sleep 2
        $0 start
        ;;
  status)
        pid=`ps ax | grep -i 'org.apache.zookeeper' | grep -v grep | awk '{print $1}'`
        if [ -n "$pid" ]
          then
          echo "Zookeeper is Running as PID: $pid"
        else
          echo "Zookeeper is not Running"
        fi
        ;;
  *)
        echo "Usage: $0 {start|stop|restart|status}"
        exit 1
esac

exit 0
```
Change the file to executable, change ownership, install, and start the service:
```
sudo chmod +x /etc/init.d/zookeeper
sudo chown root:root /etc/init.d/zookeeper
sudo update-rc.d zookeeper defaults
sudo service zookeeper start
sudo service zookeeper status
```
Create the file /etc/init.d/kafka on each server and paste in the following contents:
```
#!/bin/bash
#/etc/init.d/kafka
DAEMON_PATH=/opt/kafka/bin
DAEMON_NAME=kafka
# Check that networking is up.
#[ ${NETWORKING} = "no" ] && exit 0

PATH=$PATH:$DAEMON_PATH

# See how we were called.
case "$1" in
  start)
        # Start daemon.
        pid=`ps ax | grep -i 'kafka.Kafka' | grep -v grep | awk '{print $1}'`
        if [ -n "$pid" ]
          then
            echo "Kafka is already running"
        else
          echo "Starting $DAEMON_NAME"
          $DAEMON_PATH/kafka-server-start.sh -daemon /opt/kafka/config/server.properties
        fi
        ;;
  stop)
        echo "Shutting down $DAEMON_NAME"
        $DAEMON_PATH/kafka-server-stop.sh
        ;;
  restart)
        $0 stop
        sleep 2
        $0 start
        ;;
  status)
        pid=`ps ax | grep -i 'kafka.Kafka' | grep -v grep | awk '{print $1}'`
        if [ -n "$pid" ]
          then
          echo "Kafka is Running as PID: $pid"
        else
          echo "Kafka is not Running"
        fi
        ;;
  *)
        echo "Usage: $0 {start|stop|restart|status}"
        exit 1
esac

exit 0
```

Save and close the file.

Change the file to executable, change ownership, install, and start the service:
```
sudo chmod +x /etc/init.d/kafka
sudo chown root:root /etc/init.d/kafka
sudo update-rc.d kafka defaults
sudo service kafka start
sudo service kafka status
```
## Create a Topic
Use the following command to create a topic named test:
```
./bin/kafka-topics.sh --zookeeper zookeeper1:2181/kafka --create --topic test --replication-factor 1 --partitions 3
```
Use the following command to describe the topic:
```
./bin/kafka-topics.sh --zookeeper zookeeper1:2181/kafka --topic test --describe
```