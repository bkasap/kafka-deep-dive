## Kafka Advanced Configuration

# Advanced Producers

## Idempotent Producers

To ensure message safety — meaning the message reaches its destination — the idempotence setting is by far the easiest and best configuration option for your producer. It ensures no duplicate messages occur from the producer and the broker, and along with other settings, like the acks=all, you can guarantee zero data loss. In this lesson, we'll go over how to configure this with our sample Java app from previous lessons.

## Batch Compression

By default, Kafka will send messages as soon as it can. To improve the efficiency and throughput of messages to the broker, batching with compression can be the quickest way to process thousands of messages. In combination with the previous lesson, this lesson on batch compression will optimize the producer for getting messages to their intended destination faster than ever before.

## Serializer

For your producer configuration, you can use the generic serializers like the Avro Serializer or the String Serializer, but you can also create a custom serializer too. As a warning, this could get hard to configure over time, as you may have to fix compatibility issues between serialization and deserialization. In this lesson, we'll go over the pieces of a custom serializer.

## Producer Buffer Memory

If your producer is producing messages too fast, to where your broker cannot keep up, the producer buffer memory may fill up. This will cause an exception error and means you should beef up your brokers or check for broker failure. Changing ```the max.block.ms``` setting is possible but not recommended based on the messages that will accrue and then not be delivered as a result.

# Advanced Consumers

## Reading Duplicate Messages

Since consumers poll for data, this allows us to control many actions at the consumer level. To protect against consumer failure, and the possibility of duplicate messages, we can insert a unique ID in our code, so that if a duplicate message is read, it is skipped and not committed twice. In this lesson, we go over how the consumer commits offsets and how to protect against duplicating messages.

Auto-commit set to true:

```java
while(true){
    List<Records> batch = consumer.poll(Duration.ofMillis(100))
    doSomethingSynchronous(batch)
}
```

Auto-commit set to false:
```java
while(true){
    batch += consumer.poll(Duration.ofMillis(100))
    if isReady(batch) {
        doSomethingSyncronous(batch)
        consumer.commitSync();
    }
}
```
Insert a unique ID for the message from within poll loop:
```java
while(true){
            ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100));

            for (ConsumerRecord<String, String> record: records){
                String id = record.topic() + record.partition() + record.offset();
                IndexRequest indexRequest = new IndexRequest(
                    "topic",
                    "messages"
                    "id"
                ).source(record.value(), XContentType.JSON);

                IndexResponse indexResponse = client.index(indexRequest, RequestOptions.DEFAULT);
                String id = indexResponse.getId();
                logger.info(id);
            }
        }
```
## Tracking Offsets

Consumers work in a group to better coodinate the subscription of messages. Tracking offsets is done by a coordinator, so the load is not taken away from the consumer itself. Once each consumer has been assigned a leader partition, any remaining consumers will sit idle. In this lesson, we cover the many configurations and optimizations you can make to your consumer to make sure the consumers are not missing any messages and they are able to quickly locate the offsets.

```java
Consumer class:

package com.github.chadmcrowell;

import java.io.IOException;
import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Consumer {

    public static KafkaConsumer<String, String> createConsumer(String topic) {

        String bootstrapServers = "kafka1:9092";
        String groupId = "application1";

        // consumer configs
        Properties properties = new Properties();
        properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

        // create consumer
        KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(properties);
        consumer.subscribe(Arrays.asList(topic));
        return consumer;
    }

    public static void main(String[] args) throws IOException {
        Logger = LoggerFactory.getLogger(Consumer.class.getName());
        RestHighLevelClient client = createClient();

        KafkaConsumer<String, String> consumer = createConsumer("topic_messages");

        // poll for data
        while(true){
            ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100));

            for (ConsumerRecord<String, String> record: records){
                String id = record.topic() + record.partition() + record.offset();
                IndexRequest indexRequest = new IndexRequest(
                    "topic",
                    "messages",
                    "id"
                ).source(record.value(), XContentType.JSON);

                IndexResponse indexResponse = client.index(indexRequest, RequestOptions.DEFAULT);
                String id = indexResponse.getId();
                logger.info(id);
            }
        }

        client.close();
    }
}
```
## Partition Rebalancing

The way partitions are assigned to consumers depends on the strategy you choose (if you choose one at all). A rebalance occurs when a consumer is reassigned because it's either dead or added to a new consumer group. In this lesson, we discuss the many ways of assigning partitions to consumers and how you can prevent rebalancing.

Consumer rebalance listener:
```java
public class MaintainOffsetsOnRebalance
 implements ConsumerRebalanceListener {

    public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
        for(TopicPartition partition: partitions)
            saveOffsetInStorage(consumer.position(partition));
    }

    public void onPartitionsAssigned(Collection<TopicPartition> partitions) {
        for(TopicPartition partition: partitions)
            consumer.seek(partition, readOffsetFromStorage(partition));
    }
}
```
## Consumer Group Coordinator

One of the brokers in the Kafka cluster is assigned the role of "group coordinator." This role is essential in keeping the consumer group running smoothly. It has many responsibilities, including reassigning consumers different tasks in the group when consumers are added or removed. In this lesson, we cover the details of how this coordinator role is selected and maintained.

# Advanced Topics

## Topic Design

In this lesson, we go through some design considerations when creating a topic. Why is this important? Well, depending on the needs of your data, you may need to create different topics for different types. Also, think about what information you need to analyze versus what data you could ignore. This will help with the speed of the consumers and processing most efficiently.

## Topic Options

In this lesson, we explore the many different options when creating a topic. Also, being able to view the logs is important for troubleshooting purposes.

Create a topic only if a topic with the same name doesn't exist:
```
bin/kafka-topics.sh --zookeeper zookeeper1:2181/kafka --create --topic test_topic --replication-factor 2 --partitions 2 --if-not-exists
```
List the topics:
```
bin/kafka-topics.sh --zookeeper zookeeper1:2181/kafka --list
```
Describe the topic:
```
bin/kafka-topics.sh --zookeeper zookeeper1:2181/kafka --topic test_topic --describe
```
Alter the number of partitions:
```
bin/kafka-topics.sh --zookeeper zookeeper1:2181/kafka --alter --topic test_topic --partitions 3
```
Delete a topic:
```
bin/kafka-topics.sh --zookeeper zookeeper1:2181/kafka --delete --topic test_topic
```
Create a topic with more replications than available brokers:
```
bin/kafka-topics.sh --zookeeper zookeeper1:2181/kafka --create --topic test_topic --replication-factor 4 --partitions 2
```
Dump the log segments:
```
bin/kafka-run-class.sh kafka.tools.DumpLogSegments --print-data-log --files /data/kafka/topic-1-0/00000000000000000001.log
```
Rotate the logs:
```
bin/kafka-configs.sh --zookeeper zookeeper1:2181/kafka --alter --entity-type topics --entity-name topic-1 --add-config segment.ms=60000
```

## Topic Alterations

In this lesson, we talk about log compaction and explore why you would or wouldn't want to use it within your Kafka cluster.

Create a topic with compaction:
```
bin/kafka-topics.sh --zookeeper zookeeper1:2181/kafka --create \
--topic compact_test_topic --replication-factor 2 --partitions 2 \
--config cleanup.policy=compact 
```